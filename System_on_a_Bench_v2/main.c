/****************************************************************************
 *
 * Authors: Arturo Gamboa Gonzales, Sutter Lum, Sriram Venkataraman
 *
 * Description: Source file for the HSL Drive-by-Wire Senior Design Project
 *
 *****************************************************************************/

/* ============================================================================
 *                           INCLUDE SECTION
 * ==========================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "components.h"
#include "pit_lld_cfg.h"
#include "saradc_lld_cfg.h"
#include "pwm_lld_cfg.h"
#include "spi_lld_cfg.h"
#include "serial_lld_cfg.h"
#include "main.h"
#include "LEDs.h"
#include "PID.h"
#include "packet_builder.h"
//#include "FreeRTOS.h"
//#include "task.h"

#define ADC_CHAN 63U
#define TICKS_PER_ROT 819
#define TICK_PERIOD 100 //10 kHz
#define DELAY 0
#define TRIGGER 0
#define CH0 0
#define CH1 1
#define SPI_LEN 2
#define CLEAR_BUF(buf, len) for(uint8_t i_macro; i_macro < len; i_macro++) buf[i_macro] = 0;
#define PACKET_LEN 6

/* ============================================================================
 *                       PRIVATE VARIABLES SECTION
 * ==========================================================================*/

static volatile InputMode inputMode = SYSTEMOFF;
static volatile Bool inMotion = False;
static volatile Bool resetWasPressed = False;
static volatile Bool systemOFF = False;
static Bool error = False;
static CarState currentState = START;
static uint8_t rxBuf[RX_LEN]; // Serial receive buffer
static uint8_t rxIndex;
static volatile uint16_t adcVal; // ADC value
static volatile uint8_t isDataReady; // ADC flag
static uint8_t pitFlag; // Timer flag
static uint8_t spiBuf[SPI_LEN]; // SPI transmit Buffer

/* ============================================================================
 *                       PRIVATE FUNCTIONS SECTION
 * ==========================================================================*/

	/*
 * Description: Creates the SPI message for the DAC
 * Arguments:
 *   buf - buffer to be written with the message
 *   level - the base 1024 voltage level
 * 
 * Return:
 *   None
 */
void dac_message(uint8_t *buf, uint16_t level){
	level = level & 0x3FF;
	buf[0] = 0x10 + (level >> 6);
	buf[1] = (level << 2) & 0xFF;
}


/*
 * Description: Commands the accelerator using a DAC
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *   SUCCESS if DAC is connected, FAILURE if disconnnected
 */
Bool update_throttle(CarValues* current, CarValues* prev) {
	// print('setting throttle')
	return SUCCESS;
}

/*
 * Description: Commands the brake actuator to the desired position
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  SUCCESS if actuator is connected, FAILURE if disconnnected
 */
Bool update_braking(CarValues* current, CarValues* prev) {
	// print('setting brake')
	return SUCCESS;
}

/*
 * Description: Commands the steering actuator to the desired position
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 * Return:
 *   SUCCESS if actuator is connected, FAILURE if disconnnected
 */
Bool update_steering(CarValues* current, CarValues* prev) {
	// print('setting steering')
	return SUCCESS;
}

/*
 * Description: Toggles the left turn signal
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  NULL
 */
void update_left_turn_signal(CarValues* current) {
	return;
}

/*
 * Description: Toggles the right turn signal
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  NULL
 */
void update_right_turn_signal(CarValues* current) {
	return;
}
/*
 * Description: Computes the nth Fibonacci number
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  NULL
 */
void update_horn(CarValues* current) {
	return;
}

/*
 * Description: Computes the nth Fibonacci number
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  NULL
 */
void update_wind_shield_wipers(CarValues* current) {
	return;
}

int main(void) {
	
	// START FEEDBACK VARS //
	int8_t dutyCycle; // The duty cycle of the h-bridge motor driver
	uint32_t curPos; // The current steering column position
	int32_t err; // 
	float u_p,u_i, u0, a0, a1;
	// END FEEDBACK VARS//
	uint32_t cmdPos = 2048; // Value to be passed to PID functions
	isDataReady = 0; // initializing ADC Flag
	adcVal = 0; // ADC adcVal
	rxIndex = 0;
	pitFlag = FALSE;
	// Initialize buffers to zero
	CLEAR_BUF(spiBuf, SPI_LEN);
	CLEAR_BUF(rxBuf, RX_LEN);

	/* Initialization of all the imported components in the order specified in
	 the application wizard. The function is generated automatically.*/
	componentsInit();

	/* Enable Interrupts */
	irqIsrEnable();



	// create the containers for values
	CarValues* newVals = car_values_init();

	CarValues* prevVals = car_values_init();

	// GAMEPAD init (serial)
	sd_lld_start(&SD1, &serial_config_packet_cfg);

	// initialize LEDs
	system_off_LED();

	// timer to read input buttons
	pit_lld_start(&PITD1, pit0_config);
	pit_lld_channel_start(&PITD1,PIT0_CHANNEL_CH1);

	// Enable PWM Group
	pwm_lld_init();
	pwm_lld_start(&PWMD9, &pwm_config_h_bridge);

	// Enable SARADC
	saradc_lld_start(&SARADC12DSV, &saradc_config_steering_col);
	saradc_lld_start_conversion(&SARADC12DSV);

	while(!isDataReady);
	isDataReady = FALSE;

	printf("ADC: %d\r\n", adcVal);
	PID_init(adcVal);

	PID_setPosition(2000);

	spi_lld_start(&SPID1, &spi_config_mcp4812);

	dac_message(spiBuf, 512);
	spi_lld_send(&SPID1, 2, spiBuf);

	while (1) {


		// Check rx buffer
		while((int)SD1.rx_write_ptr != (int)SD1.rx_read_ptr && rxIndex < PACKET_LEN)
			rxIndex += sd_lld_read(&SD1, rxBuf + rxIndex, 1);
//		if(rxIndex == 6){
//			packet2values(rxBuf, newVals);
//			CLEAR_BUF(rxBuf, 6);
//			rxIndex = 0;
//			printf("%f     %f       %f\n", newVals->steering_angle, newVals->percent_throttle, newVals->percent_brake);
//		}

		switch (currentState) {

		/**********************************************************************************
		* START State: Vehicle stays here until a switch is pressed (indicated by inputMode)
		***********************************************************************************/
			case (START):

			if (inputMode == GAMEPAD || inputMode == LAPTOP) {

				if (inMotion == True) {
					currentState = ERROR;
					system_off_LED();
				} //don't enable the system if the car is in motion

				else currentState = SETUP;

			}
			break;

		/**********************************************************************************
		* SETUP State: Perform necessary actions to put vehicle in OPERATION state
		***********************************************************************************/
			case (SETUP):

			error = False;

			// check power on HV step down circuit, if isolation fault error = 1
			// run brake actuator test, if no response error = 1
			// run steering actuator test, if no response error = 1

			if (error == False) {
				currentState = OPERATION;
				(inputMode == GAMEPAD) ? gamepad_LED() : laptop_LED();
			} else {
				currentState = ERROR;
				system_off_LED();
			}
			break;

		/**********************************************************************************
		* OPERATION State: The vehicle will remain in this state under nominal conditions
		***********************************************************************************/
			case (OPERATION):

			if (systemOFF == True) {
				currentState = ERROR;
				system_off_LED();
				break;
			}
			// check if the laptop is connected, and parse the values
			if (inputMode == LAPTOP && rxIndex == PACKET_LEN) {
				// change sd1 to whatever serial is connected to the joystick
				if (packet2values(rxBuf, newVals) == RELEASE) {
					currentState = ERROR;
					system_off_LED();
					break;
				}
				CLEAR_BUF(rxBuf, 6);
				rxIndex = 0;
			}
			// check if the gamepad is connected, and parse the values
			if (inputMode == GAMEPAD && rxIndex == PACKET_LEN) {
				packet2values(rxBuf, newVals);
				CLEAR_BUF(rxBuf, 6);
				rxIndex = 0;
				printf("%f     %f       %f\n", newVals->steeringAngle, newVals->percentThrottle, newVals->percentBrake);
				cmdPos = 1000 + (newVals->steeringAngle) * 2 / 5;
				dac_message(spiBuf, newVals->percentThrottle * 10);
				spi_lld_send(&SPID1, 2, spiBuf);
//				if (packet2values(rx_buf, newVals) == RELEASE) {
//					currentState = ERROR;
//					system_off_LED();
//					break;
//				}
			}
			if ( (newVals->ESTOP) == True) {
				currentState = ERROR;
				system_off_LED();
				break;
			}

			if (update_throttle(newVals, prevVals) == FAILURE) {
				currentState = ERROR;
				system_off_LED();
				break;
			}

			if (update_braking(newVals, prevVals) == FAILURE) {
				currentState = ERROR;
				system_off_LED();
				break;
			}

			if (update_steering(newVals, prevVals) == FAILURE) {
				currentState = ERROR;
				system_off_LED();
				break;
			}

			update_left_turn_signal(newVals);

			update_right_turn_signal(newVals);

			update_horn(newVals);

			update_wind_shield_wipers(newVals);

			prevVals = newVals;

			break;

		/**********************************************************************************
		* ERROR State: The system will remain idle in this state until the reset is pressed
		***********************************************************************************/
			case (ERROR):
			if (resetWasPressed == True) {
				currentState = START;
				inputMode = SYSTEMOFF;
				resetWasPressed = False;
				car_values_reset(newVals);
				car_values_reset(prevVals);
			}
			break;

		/**********************************************************************************
		* Update the display with pertinent information
		***********************************************************************************/
		//figure out i2c coms for isr

		}
		if(pitFlag){
			while(!isDataReady);
//			control = PID_update(value);
//
//			printf("ADC: %d\r\n", control);
//
//			if(control > 0){
//				pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, 0, DELAY, TRIGGER);
//				pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, control, DELAY, TRIGGER);
//			}
//			else{
//				pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, 0, DELAY, TRIGGER);
//				pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, abs(control), DELAY, TRIGGER);
//			}

			curPos = adcVal;
			err = cmdPos - curPos;
			u_p = 1.8 * (float)err;
			a0 = a1 + (float)err * 0.05;
			u_i = (0.06 * a0);
			u0 = u_p + u_i;
			if(abs(u0) > TICKS_PER_ROT){
				a0 = a1;
				u_i = 0;
				u0 = (u0 > 0)? TICKS_PER_ROT : -TICKS_PER_ROT;
			}

			dutyCycle = (uint8_t) 100 * abs(u0) / TICKS_PER_ROT;

		  // printf("ADC: %ld, cmd: %ld, u0: %f\r\n", curPos, cmdPos, u0);

			if(u0 > 0){
				pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, 0, DELAY, TRIGGER);
				pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, dutyCycle, DELAY, TRIGGER);
			}
			else{
				pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, 0, DELAY, TRIGGER);
				pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, dutyCycle, DELAY, TRIGGER);
			}


			a1 = a0;

			pitFlag = FALSE;
			isDataReady = FALSE;
		}
	}

}

/* ============================================================================
 *                           ISR FUNCTIONS
 * ==========================================================================*/

/*
 * Description: Checks buttons on the mode select panel
 * Arguments:
 *  void
 *
 * Return:
 *  void
 */
void pit_isr(void){
	pitFlag = TRUE;

	int gamepadTriggered = pal_lld_readpad(GAMEPAD_BUTTON_PORT, GAMEPAD_BUTTON_PIN);

	int laptopTriggered = pal_lld_readpad(LAPTOP_BUTTON_PORT, LAPTOP_BUTTON_PIN);

	if (pal_lld_readpad(OFF_BUTTON_PORT, OFF_BUTTON_PIN) == 1) systemOFF = True;

//	if (pal_lld_readpad(RESET_BUTTON_PORT, RESET_BUTTON_PIN) == 1) resetWasPressed = True;

	if (gamepadTriggered && laptopTriggered) return;
	else if (gamepadTriggered || laptopTriggered) inputMode = gamepadTriggered ? GAMEPAD : LAPTOP;

}


/*
 * Description: Reads the adc value and sets a flag
 *              saying there is new data
 * Arguments:
 *  saradcp - the SARADC Driver
 *
 * Return:
 *  void
 */
void steering_col_adc_isr(SARADCDriver *saradcp){
	adcVal = saradc_lld_readchannel(saradcp, ADC_CHAN);
	isDataReady = 1;
}