/*
 * serial.c
 *
 *  Created on: Apr 21, 2022
 *      Author: artur
 */


#include "components.h"
#include "CircularBuffer.h"

static uint8_t buffer[CIRCULAR_LEN];
static uint16_t readPtr;
static uint16_t writePtr;

void circular_init(void){
	uint16_t i;
	readPtr = 0;
	writePtr = 0;
	for(i = 0; i < CIRCULAR_LEN; i++)
		buffer[i] = 0;
}

uint16_t circular_append(uint8_t *buf, uint16_t len){
	uint16_t i;
	for(i = 0; i < len; i++){
		buffer[writePtr] = buf[i];
		writePtr++;
		if(writePtr >= CIRCULAR_LEN)
			writePtr = 0;
	}
	if(writePtr >= readPtr && writePtr != CIRCULAR_LEN - 1)
		readPtr++;
	return (len > CIRCULAR_LEN)? CIRCULAR_LEN : len;
}

uint16_t circular_getLen(void){
	uint16_t len, i;
	if(writePtr >= readPtr)
		return writePtr - readPtr;
	for(i = readPtr, len = 0; len < CIRCULAR_LEN; len++){
		if(i == writePtr)
			return len;
		i++;
		if(i >= CIRCULAR_LEN)
			i = 0;
	}
	return len;
}

uint16_t circular_getBuf(uint8_t *buf, uint16_t len){
	uint16_t i, j;
	for(i = 0, j = readPtr; i < len && j != readPtr; i++){
		buf[i] = buffer[j];
		j++;
		if(j >= CIRCULAR_LEN)
			j = 0;
	}
	return (i > CIRCULAR_LEN)? CIRCULAR_LEN : i;
}

void circular_clear(void){
	circular_init();
}
